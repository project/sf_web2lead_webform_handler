<?php

namespace Drupal\sf_web2lead_webform_handler\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * SalesForce Web2Lead Webform handler.
 *
 * @WebformHandler(
 *   id = "sf_web2lead_webform_handler",
 *   label = @Translation("SalesForce Web2Lead Webform Handler"),
 *   category = @Translation("Transaction"),
 *   description = @Translation("Sends the submission data to SalesForce Lead."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class SalesforceLeadWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'salesforce_oid' => '',
      'salesforce_url' => '',
      'debug' => '',
      'debug_email' => '',
      'extra_values' => '',
      'fields_mapping' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();
    $form = parent::buildConfigurationForm($form, $form_state);

    $this->applyFormStateToConfiguration($form_state);

    // API settings.
    $form['api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SalesForce API settings'),
      '#attributes' => ['id' => 'salesforcelead-webform-handler--api-settings'],
    ];

    $form['api_settings']['salesforce_oid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce OID'),
      '#default_value' => $this->configuration['salesforce_oid'],
    ];

    $form['api_settings']['salesforce_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salesforce URL'),
      '#default_value' => $this->configuration['salesforce_url'] ? $this->configuration['salesforce_url'] : 'https://webto.salesforce.com/servlet/servlet.WebToLead',
    ];

    $form['api_settings']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('DEBUG MODE'),
      '#default_value' => $this->configuration['debug'] ? $this->configuration['debug'] : 0,
    ];

    $form['api_settings']['debug_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Debug email'),
      '#default_value' => $this->configuration['debug_email'] ? $this->configuration['debug_email'] : 0,
      '#states' => [
        'visible' => [
          ':input[name="debug"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['api_settings']['extra_values'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extra values'),
      '#default_value' => $this->configuration['extra_values'],
      '#description' => $this->t('Define extra values to map. One value per line. Only after saving this handler, values will be available in the mappings-field to map with a SalesForce-field.'),
    ];

    $form['api_settings']['fields_mapping'] = [
      '#type' => 'webform_mapping',
      '#title' => $this->t('Fields mapping'),
      '#title_display' => 'invisible',
      '#webform_id' => $webform->id(),
      '#required' => FALSE,
      '#description' => $this->t('Please map the webform fields to your Salesforce Lead fields. Use a pipe to map to multiple fields.'),
      '#description_display' => 'before',
      '#default_value' => $this->configuration['fields_mapping'],
      '#source' => $this->getWebformMappingOptions(),
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state,  WebformSubmissionInterface $webform_submission) {
    // Get the submission data.
    $values = $webform_submission->getData();
    $post_vars = $this->createMergeVars($values);

    if (!empty($post_vars)) {
      $post_vars['oid'] = $this->configuration['salesforce_oid'];
      $post_vars['debug'] = $this->configuration['debug'];
      $post_vars['debugEmail'] = $this->configuration['debug_email'];
      $post_vars['retURL'] = '';
      $post_vars['encoding'] = 'UTF-8';

      $client = \Drupal::service('http_client');

      $options = [];
      $options['headers'] = [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ];
      $options['form_params'] = $post_vars;

      try {
        $response = $client->request('POST', $this->configuration['salesforce_url'], $options);
      }
      catch (GuzzleException $e) {
        $response = $e->getResponse();
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getWebformMappingOptions() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getWebform();
    $options = [];
    $elements = $webform->getElementsInitializedFlattenedAndHasValue('view');

    // Replace tokens which can be used in an element's #title.
    /** @var \Drupal\webform\WebformTokenManagerInterface $token_manager */
    $token_manager = \Drupal::service('webform.token_manager');
    $elements = $token_manager->replace($elements, $webform);

    foreach ($elements as $key => $element) {
      $title = $element['#admin_title'] ?: $element['#title'] ?: $key;
      $options[$key] = $title . ' [' . $key . ']';
    }

    // Add extra custom values mapping.
    $extra_values = $this->configuration['extra_values'];
    if (isset($extra_values) && !empty($extra_values)) {
      // Split by newline.
      $values = preg_split('/\r\n|\r|\n/', $extra_values);

      foreach($values as $value) {
        $options[$value] = $this->t('Value:') . ' ' . $value;
      }
    }

    return $options;
  }

  /**
   * Build the Merge Vars array.
   *
   * @param $values
   */
  protected function createMergeVars($values) {
    $merge_vars = [];

    $field_mapping = $this->configuration['fields_mapping'];

    foreach ($field_mapping as $submission_key => $destination_key) {
      if (isset($values[$submission_key]) && $values[$submission_key] != '') {
        // Multiple destinations can be set with a pipe.
        $destination_keys = explode('|', $destination_key);

        foreach ($destination_keys as $destination_key) {
          $merge_vars[$destination_key] = $values[$submission_key];
        }
      }
      else {
        // Add extra mapping values.
        $merge_vars[$submission_key] = $destination_key;
      }
    }

    return $merge_vars;
  }

}
