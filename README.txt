SalesForce Web2Lead Webform Handler
===========
SalesForce Web2Lead Webform Handler provides a new Webform Handler plugin to send submission data to SalesForce via their API.


Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8


Configuration
------------

Add a new handler of type "SalesForce Web2Lead Webform Handler" to the webform.
Add your OID and map the necessary fields.
